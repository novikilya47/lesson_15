export const findProduct = (data) => ({type: 'FIND_PRODUCT', data: data});
export const resetState = () => ({type: 'RESET_PRODUCT'});
export const addProduct = (data) => ({type: 'ADD_PRODUCT', data});
export const editProduct = (data) => ({type: 'EDIT_PRODUCT', data});
export const deleteProduct = (id) => ({type: 'DELETE_PRODUCT', id});
export const editUser = (data) => ({type: 'EDIT_USER', data});
export const fetchUsersSuccess = (data) => ({type:'FETCH_USER_SUCCESS', data});