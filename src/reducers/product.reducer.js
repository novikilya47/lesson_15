const defaultState = [
    {
        name:'iPhone', 
        id:1,
        img:"https://i.citrus.ua/uploads/shop/9/6/9601bc1ab5df854d950ccbc255a6b5ec.jpg",
        discription: "5G speed. A14 Bionic, the fastest chip in a smartphone. An edge-to-edge OLED display. Ceramic Shield with four times better drop performance. And Night mode on every camera. iPhone 12 has it all — in two perfect sizes.",
        price: "599$"
    },
    {
        name:'MacBook', 
        id:2,
        img:"https://clickit.by/wp-content/uploads/2021/01/201210170015136250-2.png",
        discription: "Our thinnest, lightest notebook, completely transformed by the Apple M1 chip. CPU speeds up to 3.5x faster. GPU speeds up to 5x faster. Our most advanced Neural Engine for up to 9x faster machine learning. The longest battery life ever in a MacBook Air. And a silent, fanless design. This much power has never been this ready to go.",
        price: "1299$"
    }, 
    {
        name:'Apple Watch', 
        id:3,
        img:"https://redstore.by/wp-content/uploads/2020/10/MTPM2_VW_34FRwatch-44-alum-gold-nc-6s_VW_34FR_WF_CO.png",
        discription: "Measure your blood oxygen level with a revolutionary sensor and app. Take an ECG anytime, anywhere. See your fitness metrics at a glance with the enhanced Always-On Retina display. With Apple Watch Series 6 on your wrist, a healthier, more active, more connected life is within reach.",
        price: "399$"
    }]
export default function reducer (state = defaultState, action) {
    switch (action.type) {
        case 'FIND_PRODUCT':
            return state.filter(product=>product.name.toLowerCase().includes(action.data.toLowerCase()));
        case 'DELETE_PRODUCT':
            return state.filter((el)=>el.id !==action.id);
        case 'ADD_PRODUCT':
            return[...state, action.data];
        case 'EDIT_PRODUCT':
        let newState = [];
        state.forEach((el) => {
            if(el.id === action.data.id) {
                newState.push(action.data)
            } else {
                newState.push(el)
            }});
        return newState;
        case 'RESET_PRODUCT':
            return defaultState
        default:
            return state;
    }
};