import { getUsersQuery } from "../services/user.service";
import { fetchUsersSuccess } from "../actionCreators/action";

export const getUsers = () => (dispatch) => {
    getUsersQuery()
        .then((response) => response.json())
        .then((data) => {
            dispatch(fetchUsersSuccess(data))
        })
        .catch((err) => {
            console.log(err)
        })
};
