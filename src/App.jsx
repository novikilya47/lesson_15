import {Provider} from 'react-redux';
import {store} from "./store/store";
import Info from "./Info/Info";
import Admin from "./Admin/Admin";
import User from "./User/User";
import Home from "./Home/Home";
import Navbar from "./Navbar";
import {BrowserRouter} from 'react-router-dom';
import { Route } from "react-router-dom";

function App() {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <Navbar />
                <Route path='/home' component={Home}/>
                <Route path='/admin' component={Admin}/>
                <Route path='/user' component={User}/>
                <Route path='/info' component={Info}/>
            </BrowserRouter>
        </Provider>
    );
}

export default App;
