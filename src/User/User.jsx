import React, {useState} from 'react';
import {connect} from "react-redux";
import {findProduct, resetState} from "../actionCreators/action";

function User(props) {
    const [name, setName] = useState(props.product.name);

    const handleFind = () => {
        props.findProduct(name)
    }

    const reset = () => {
        props.resetState()
    }
        
        return (
            <div className="User">
                <div className="User_text">Навигация по имени</div>
                <textarea className="User_textarea" value={name} onChange={(e)=>setName(e.target.value)}></textarea>
                <div className="Search_buttons">
                    <button onClick={handleFind}>Поиск</button>
                    <button onClick={reset}>Вернуть состояние</button> 
                </div>            
                <div className = "Product_block">
                    {props.product.map(product => (
                            <div className="Product"> 
                                <div className="Product_obert">
                                    <div className="Product_title">{product.name}</div>
                                    <img className="Product_img" src = {product.img} alt = "картинка"></img>
                                    <div className="Product_discription">{product.discription}</div>
                                    <div className="Product_price">{product.price}</div> 
                                    <div className="Product_buttons">
                                        <button>Купить</button>
                                    </div> 
                                </div>  
                            </div>
                        ))}
                </div>
           </div>
        )
}

const mapStateToProps = (state) => ({
    product: state.product
});

const mapDispatchToProps = (dispatch) => ({
    findProduct: (data) => {dispatch(findProduct(data))},
    resetState: () => {dispatch(resetState())},
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(User);
