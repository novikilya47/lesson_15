import {createStore, combineReducers, applyMiddleware} from "redux";
import infoReducer from '../reducers/info.reducer';
import productReducer from '../reducers/product.reducer';
import thunk from "redux-thunk";

export const rootReducer = combineReducers({
    info: infoReducer,
    product: productReducer,
});

export const store = createStore(rootReducer, applyMiddleware(thunk));
