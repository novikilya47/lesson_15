import React, {useState, useEffect} from 'react';
import {connect} from "react-redux";
import {editUser} from "../actionCreators/action";
import {getUsers} from "../thunk/user.thunk"

function Info(props) {
    const [name, setName] = useState(props.info.name);
    const [email, setEmail] = useState(props.info.email);

    useEffect(() => {
        props.getUsers();
    }, []);

    const handleEdit = () => {
        let user = {
            name: name ? name : props.info.name,
            email: email ? email : props.info.email
        };
        props.editUser(user);
    }
    
    return (
        <div className="Info">
            <div>Имя: {props.info.name}</div>
            <div>Email: {props.info.email}</div>
            <div className="Info_inputs">
                <div className="Info_input"><p>Имя</p><textarea type='text' onChange={(e) => setName(e.target.value)}/></div>
                <div className="Info_input"><p>Email</p><textarea type='text' onChange={(e) => setEmail(e.target.value)}/>  </div>
            </div>  
            <button onClick={() => handleEdit()}>Изменить информацию</button>   
        </div>
    )
}

const mapStateToProps = (state) => ({
   info: state.info
});

const mapDispatchToProps = (dispatch) => ({
    editUser: (data) => {dispatch(editUser(data))},
    getUsers:()=> dispatch(getUsers()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Info);
